# Autogrow Textarea Drupal Module

Automatically adjust textarea height based on user input.

While CKEditor will automatically adjust the height of textareas,
non-CKEditor textareas do not have this ability. This module adds
that functionality to non-CKEditor textareas.

For a full description of the module, visit the project page:
[project page](https://www.drupal.org/project/autogrow_textarea)

To submit bug reports and feature suggestions, or to track changes:
[issue queue](https://www.drupal.org/project/issues/autogrow_textarea)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Rob Loach - [RobLoach](https://www.drupal.org/u/RobLoach)
- Emanuel Greucean - [gremy](https://www.drupal.org/u/gremy)
